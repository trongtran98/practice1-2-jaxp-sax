/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practice1;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author TrongTran
 */
public class Prac1_JAXP_DOM {

    public static void main(String[] args) {
        countStudents();
    }

    public static void countStudents() {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = dbf.newDocumentBuilder();
            File file = new File("src\\practice1\\student.xml");
            //load xml data into memory
            Document doc = builder.parse(file);
            //System.out.println("root name " + doc.getDocumentElement().getNodeName());
            //get list of Student node
            NodeList students = doc.getElementsByTagName("student");
            System.out.println("StudentID\tStudentName");
            //loop each node
            for (int i = 0; i < students.getLength(); i++) {
                //get node
                Node temp = students.item(i);
                //get childnode
                NodeList childs = temp.getChildNodes();
                
                for (int j = 0; j < childs.getLength(); j++) {
                    //loai bo phan tu khoang trang ket thuc
                    if (!childs.item(j).getNodeName().equals("#text")) {
                        System.out.print(childs.item(j).getTextContent() + "\t\t");
                    }
                }
                System.out.println();
            }
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            ex.printStackTrace();
        }
    }
}
