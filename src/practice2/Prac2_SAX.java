/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practice2;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author TrongTran
 */
public class Prac2_SAX extends DefaultHandler {

    static int count = 0;
    static String name = "";

    public static void main(String[] args) {
        countUsers();
        infoStudents();
        getRole("admin");
    }

    public static void countUsers() {
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser parser = factory.newSAXParser();
            File file = new File("C:\\apache-tomcat-8.0.50\\conf\\tomcat-users.xml");
            parser.parse(file, new Prac2_SAX());
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            ex.printStackTrace();
        }
        System.out.println("Total users in tomcat-user.xml: " + count);
        System.out.println("--------------------------------------");
    }

    public static void infoStudents() {
        System.out.println("id\tname");
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser parser = factory.newSAXParser();
            File file = new File("src\\practice2\\student.xml");
            parser.parse(file, new Prac2_SAX());
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            ex.printStackTrace();
        }
        System.out.println("--------------------------------------");
    }

    public static void getRole(String name) {
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser parser = factory.newSAXParser();
            File file = new File("C:\\apache-tomcat-8.0.50\\conf\\tomcat-users.xml");
            Prac2_SAX.name = name;
            parser.parse(file, new Prac2_SAX());

        } catch (ParserConfigurationException | SAXException | IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equals("user")) {
            count++;

            if (attributes.getValue("username").equals(name)) {
                System.out.println("Roles: "+attributes.getValue("roles"));
            }
        }
        if (qName.equals("student")) {

            System.out.println(attributes.getValue("id") + "\t" + attributes.getValue("name"));
        }

    }
}
